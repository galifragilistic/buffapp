class SlackMessage {
    constructor() {
        this.response_type = 'in_channel'
        this.text = 'Päivän lounakset'
        this.blocks = []
    }
    addDivider() { this.blocks.push(divider) }
    addSection(text) { this.blocks.push(section(text)) }
    addButtonSection(text, buttonText, value) {
        let sec = section(text)
        sec.accessory = buttonAccessory(buttonText, value)
        this.blocks.push(sec)
    }
    addVotingContext() {
        this.blocks.push(context)
    }
    attachMenuBlocks(menus) {
        let menutext = ''
        menus.forEach(e => {
            menutext += '*' + e.name + '*\n'
            e.menu.forEach((menuitem) => {
                menutext += menuitem + '\n'
            })
            this.addButtonSection(menutext, 'Vote', e.name)
            this.addVotingContext()
            this.addDivider()
            menutext = ''
        })
    }
    message(text) {
        let msg = 
            {
                response_type: 'in_channel',
                text: text
            }
        return msg
    }
}

const divider = { type: 'divider' }

const context = 
    {
        type: 'context',
        elements: [
            {
                type: 'mrkdwn',
                text: 'Ei ääniä'
            }
        ]
    }

let section = (text) => {
    let section = 
        {
            type: 'section',
            text: {
                type: 'mrkdwn',
                text: text
            }
        }
    return section
}

let buttonAccessory = (buttonText, value) => {
    let accessory = {
        type: 'button',
        text: {
            type: 'plain_text',
            text: buttonText
        },
        value: value
    }
    return accessory
}

module.exports = SlackMessage