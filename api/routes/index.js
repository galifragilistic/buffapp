const lounasRoutes = require('./lounasRoutes');

module.exports = function(app, db) {
  lounasRoutes(app, db)
  app.get('/', function (req, res) {
    res.send('Hello there!')
  });
};
