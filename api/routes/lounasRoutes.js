const axios = require('axios')
const parser = require('../modules/menuparser')
const datab = require('../modules/data')
const menulister = require('../modules/menulister')
const slackresponder = require('../modules/slackresponder')
const SlackMessage = require('../models/slackmessageModel')

const axiosconfig = {
    headers: {
        'Content-Type': 'application/json'
    }
}

module.exports = (app, db) => {
    app.post('/slackcommand', (req, res) => {
        let response_url = req.body.response_url
        let text = req.body.text

        slackresponder.getSlackMessage(text).then(message => {
            axios.post(response_url, JSON.stringify(message), axiosconfig)
            .catch(error => console.error(error))
        })

        res.send(new SlackMessage().message("Haetaan lounaslistoja..."))
    })
    app.post('/slackaction', (req, res) => {
        res.send(200)
        let payload = JSON.parse(req.body.payload)
        let response_url = payload.response_url
        let user = payload.user
        let message = payload.message
        let actions = payload.actions
        let response = slackresponder.getSlackAction(user, message, actions)
        
        if (!response) {
            return
        }

        axios.post(response_url, JSON.stringify(response), axiosconfig)
        .catch((error) => {
            console.error(error)
        })
    })

    app.get('/lounas', (req, res) => {
        menulister.getMenus().then(menus => {
            res.send(menus)
        })
    })
    app.get('/ravintolat', (req, res) => {
        datab.getRestaurants((restaurants) =>{
            res.send(restaurants)
        })
    })
    app.post('/ravintolat', (req, res) => {
        let restaurantUrl = req.body.url
        parser.parserestaurantname(restaurantUrl, (name) => {
            datab.addRestaurant(restaurantUrl, name)
            res.send('ravintola lisätty: ' + name)
        })
    })
    app.delete('/ravintolat', (req, res) => {
        datab.deleteRestaurant(req.body.url)
        res.send('ravintola poistettu')
    })
    app.delete('/removeall', (req, res) => {
        datab.getRestaurants((restaurants) => {
            restaurants.forEach(rest => {
                datab.deleteRestaurant(rest.url)
            })
        })
        res.send('kaikki ravintolat poistettu')
    })
    app.post('/createlocaldb', (req, res) => {
        datab.createDatabase()
        res.send('db luotu')
    })
    app.post('/createcollection', (req, res) => {
        datab.createCollection()
        res.send('kokoelma luotu')
    })
}