const moment = require('moment')
const axios = require('axios')
var $ = require('cheerio')

module.exports = {
    parserestaurantname: (url, callback) => {
        axios.get(url).then((response) => {
            let data = ''
            response.on('data', (chunk) => {
                data += chunk
            })

            response.on('end', () => {
                let name = parsename(data)
                callback(name)
            })
        }).on('error', (err) => {
            console.log('Error: ' + err.message)
            callback("error")
        })
    },
    parserestaurantmenu: async (url) => {
        const headers = {
            headers: {
                'User-Agent': 'request'
            }
        }

        const response = await axios.get(url, headers).catch(ex => console.log(ex))

        if (response.status === 200) {
            const curDate = moment().format('D.M') 
            const menu = parsemenu(response.data, curDate)
            return menu
        }
    }
}

customStringify = (o) => {
    let cache = [];
    const result = JSON.stringify(o, function(key, value) {
        if (typeof value === 'object' && value !== null) {
            if (cache.indexOf(value) !== -1) {
                // Duplicate reference found, discard key
                return;
            }
            // Store value in our collection
            cache.push(value);
        }
        return value;
    });
    cache = null; // Enable garbage collection

    return result
}

parsename = (htmlString) => {
    let nameElem = $('div.tile-container > div.tile-full > h2[itemprop="name"]', htmlString)
    let name = nameElem.text().trim()
    return name
}
parsemenu = (htmlString, curDate) => {
    let divs = $('div.item:has(div.item-header)', htmlString)
    let curItem = ''
    let menuitems = []

    if (!divs.length){
        return menuitems
    }

    for (let i = 0; i < 5; i++){
        curItem = divs[i]
        let datedata = $('div.item-header > h3', curItem)
        let split = datedata.text().split(' ')
        let date = split[1]
        date = date.substring(0, date.length - 1)

        if (date.toString() !== curDate.toString()){
            continue
        }

        let menuitemlist = $('div.item-body > ul > li.menu-item', curItem)
        menuitemlist.each((i, e) => {
            menuitems.push($('p.dish', e).text().trim().replace(/\s+/g, " "))
            if ($('p.info', e).text().length > 0){
                menuitems.push($('p.info', e).text().trim().replace(/\s+/g, " "))
            }
        })
    }

    return menuitems
}