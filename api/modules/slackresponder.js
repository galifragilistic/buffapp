const datab = require('../modules/data')
const menulister = require('../modules/menulister')
const SlackMessage = require('../models/slackmessageModel')

module.exports = {
    getSlackMessage: async (text) => {
        let message = ''
        switch (text) {
            case 'lista':
                message = await getRestaurantList()
                return message
            default:
                message = await getMenuSlackMessage()
                return message
            break
        }
    },
    getSlackAction: (user, message, actions) => {
        // let username = user.username
        let username = `<@${user.id}>`
        let blockid = actions[0].block_id
        return addVote(message, blockid, username)
    }
}

addVote = (message, blockid, username) => {
    let blockindex = message.blocks.findIndex(b => b.block_id === blockid)
    if (blockindex === -1) {
        console.log(`block with action id ${actionid} not found`)
        return false
    }
    let context = message.blocks[blockindex + 1]
    let usernameindex = context.elements.findIndex(e => e.text === username)
    if (usernameindex != -1) {
        // unvote
        context.elements.splice(usernameindex, 1)
        if (context.elements.length === 0) {
            // add "no votes"
            context.elements.push({
                type: 'mrkdwn',
                text: 'Ei ääniä'
            })
        }
    }
    else {
        if (context.elements.find(e => e.text === 'Ei ääniä')) {
            context.elements = []
        }

        let vote = 
            {
                type: 'mrkdwn',
                text: username
            }

        context.elements.push(vote)
    }

    message.blocks.splice(blockindex + 1, 1, context)

    console.log(`final message:\n${message}`)

    return message
}

getRestaurantList = async () => {
    let msg = new SlackMessage()
    const restaurants = await datab.getRestaurants()

    restaurants.forEach(r => {
        msg.addButtonSection(r.name, 'Poista', r._id)
    });

    return msg
}

getMenuSlackMessage = async () => {
    const menus = await menulister.getMenus()
    let msg = new SlackMessage()
    msg.text = "Päivän lounakset:"

    if (menus.length === 0) {
        return msg.message('Ei ravintoloita')
    }
    else {
        msg.attachMenuBlocks(menus)
        return msg
    }
}