const parser = require('../modules/menuparser')
const datab = require('../modules/data')

module.exports = {
    getMenus: async () => {
        let menus = []
        let pushes = 0

        const restaurants = await datab.getRestaurants().catch(ex => console.log(ex))
        if (restaurants.length == 0) {
            return menus
        }

        for (let i = 0; i < restaurants.length; i++) {
            let menu = await parser.parserestaurantmenu(restaurants[i].url)
            .catch(ex => console.log(ex))

            var m = {name:restaurants[i].name, menu: menu}
            menus.push(m)
            pushes++
            if (pushes == restaurants.length){
                return menus
            }
        }
    }
}