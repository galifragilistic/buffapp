require('dotenv').config()
const MongoClient = require('mongodb').MongoClient
const config = require('../../config')
const connstr = config.connstr

module.exports = {
    getRestaurants: () => {
        return new Promise((resolve, reject) => {
            MongoClient.connect(connstr, (err, db) => {
                if (err) {
                    reject(err)
                }
                let dbo = db.db(config.dbname)
                dbo.collection(config.collectionName).find().toArray((error, res) => {
                    if (error) {
                        reject(error)
                    }
                    db.close()
                    resolve(res)
                })
            })
        })
    },
    addRestaurant: (url, name) => {
        MongoClient.connect(connstr, (err, db) => {
            if (err) throw err
            let dbo = db.db(config.dbname)
            let obj = {url: url, name: name}
            dbo.collection(config.collectionName).insertOne(obj, (err, res) => {
                if (err) throw err
                console.log('a restaurant inserted: ' + name)
                db.close()
            })
        })
    },
    deleteRestaurant: url => {
        MongoClient.connect(connstr, (err, db) => {
            if (err) throw err
            let dbo = db.db(config.dbname)
            let query = {url: url}
            dbo.collection(config.collectionName).deleteOne(query, (err, res) => {
                if (err) throw err
                console.log('a restaurant deleted: ' + url)
                db.close()
            })
        })
    },
    createDatabase: () => {
        MongoClient.connect(connstr, (err, db) => {
            if (err) throw err
            console.log('database created!')
            db.close()
        })
    },
    createCollection: (collectionName) => {
        MongoClient.connect(connstr, (err, db) => {
            if (err) throw err
            let dbo = db.db(config.dbname)
            dbo.createCollection(config.collectionName, (err, res) => {
                if (err) throw err
                console.log('collection created!')
                db.close()
            })
        })
    }
    
}